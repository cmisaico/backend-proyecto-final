API REST Practitioner 2020

Este API REST esta diseñado para el proyecto final del Practitioner 2020 - BBVA.
Todo el codigo esta alojado en GitHub en el siguiente repositorio: [API REST adi] (https://bitbucket.org/cmisaico/backend-proyecto-final/src/master)

Introducción
Este API esta desarrollado para un supuesto app de banca movil en el que tendremos usuarios, cuentas y movimientos. Contemplamos distintos casos de uso que estan descritos mas abajo seguidos de su ruta para probarlos.

Para todos los metodos de POST y PUT tendremos que pasarle un JSON con los campos necesarios, y para metodos POST, PUT y DELETE será necesario una autentificacion. Otros metodos como los GET de un solo objeto estan implementados con hipermedia enlazando con los objetos que tengas relacionados. Todas las colecciones estan implementadas con paginación HAL.

Casos de Uso
GET:
Usuarios
apitechu/v2/users -> Retorna todas las cuenta de usuarios.
apitechu/v2/users/:id -> Devuelve todas las cuentas de usuario  filtrando por el id de usuario.

Movimientos
apitechu/v2/movements/:id -> Devuelve todas los movimientos asignadas a una cuenta de banco, pasando como parametro el id de la cuenta de banco.

Cuentas de banco
apitechu/v2/accounts/:id -> Devuelve todas las cuentas de banco asignadas a un usuario, pasando como parametro el id del usuario.


POST
Usuarios
apitechu/v2/users -> A partir de un JSON que se le pasa añade un nuevo usuario a la base de datos.

Login
apitechu/v2/login -> A partir de un JSON se realiza se realiza la validacion de DNI y Password retornado los datos del usuario.
apitechu/v2/logout -> Se realiza el cambio de estado a inactivo.

Cuentas de banco
apitechu/v2/accounts -> A partir de un JSON se realiza la creacion de una cuenta de banco asignada a un usuario.

Movimientos
apitechu/v2/movements -> A partir de un JSON se realiza la creacion de una movimiento asignada a una cuenta.

DELETE
Usuarios
apitechu/v2/users/:id -> Borramos el usuario indicado en el login, para borrar un usuario será necesario autentificarse con el username: admin y password: 123456


PUT

Cuentas de banco
apitechu/v2/accounts/:id -> Modifica la el valor saldo disponible de la cuenta pasandole un JSON de entrada con los nuevos valores.